create database cSharp;

create table client (
id_client int not null auto_increment primary key,
nombre varchar(150) not null,
apellido varchar(150) not null,
cedula varchar(13) not null unique,
fecha_client date
);

create table Factura (
id int not null auto_increment primary key,
id_client int,
idCambio int,
Monto int,
operacion enum('Peso a',' a Peso') default 'Peso a',
fecha_factura date
);

create table Cambio(
idCambio int not null auto_increment primary key unique,
tipoMoneda varchar(50) not null,
valorMoneda decimal(10,2) not null 
);

create table users(
idUser int not null auto_increment primary key unique,
nombre varchar(50) not null,
pass varchar(15) not null
);

insert into factura (idCambio,id_client,monto,fecha_factura)
values
(1,1,500,'1995-02-15'),
(2,2,1500,'1999-06-30'),
(3,3,800,'2017-07-28'),
(3,1,1000,'2019-10-22');

insert into client (nombre,apellido,cedula,fecha_client)
values
('Iverson','Rodriguez',40234442412,'1985-05-20'),
('Juan','Pablo',40234445512,'1995-05-21'),
('Ovanny','Mustafa',40234442463,'1972-10-13');

INSERT INTO Cambio (idCambio, tipoMoneda, valorMoneda) 
VALUES
(1, 'Dolar', 58.20),
(2, 'Peso Mexicano', 2.90),
(3, 'Euro', 71.84),
(4, 'Real Brasileno', 10.85),
(5, 'Franco Suizo', 65.31),
(6, 'Yuan', 8.93),
(7, 'Corona Danesa', 9.45),
(8, 'Libra Esterlina', 78.95),
(9, 'Yen', 0.55),
(10, 'Libra Escocesa', 78.95),
(11, 'Corona Noruega', 6.80);

insert into users(idUser,nombre,pass) 
values 
(1,"iver","123"),
(2,"admin","admin");

create view queryresultado as 
select id,concat(nombre, ' ' ,apellido) as 'nombreCompleto',
cedula,
tipoMoneda as 'moneda',
valorMoneda as 'tasa',
monto,
if(operacion = 1,concat(operacion,' ',tipoMoneda),concat(tipoMoneda,operacion)) as 'operacion',
if(operacion = 1,monto*valorMoneda,monto/valorMoneda) as 'resultado',fecha_factura as 'fecha' from factura 
inner join cambio on factura.idCambio = cambio.idCambio
inner join client on factura.id_client = client.id_client	

















