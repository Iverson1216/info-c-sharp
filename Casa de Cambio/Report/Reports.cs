﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Casa_de_Cambio.Report
{
    public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        public void clientes()
        {
            this.reportViewer1.LocalReport.ReportPath = @"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\Casa de Cambio\Report\ReportClient.rdlc";
            //this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("prIdClient",cl.prIdClient));
            //this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("prClientNombre", cl.prClientNombre));
            //this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("prClientCedula", cl.prClientCedula));
            Microsoft.Reporting.WinForms.ReportDataSource tabla = new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", Clientes.getListCliente());
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(tabla);

            this.reportViewer1.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
        }

        public void monedas()
        {
            this.reportViewer1.LocalReport.ReportPath = @"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\Casa de Cambio\Report\ReportCoins.rdlc";

            Microsoft.Reporting.WinForms.ReportDataSource tabla2 = new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", NewMoneda.getCoinsParam());
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(tabla2);

            this.reportViewer1.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
        }

        public void Historial()
        {
            this.reportViewer1.LocalReport.ReportPath = @"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\Casa de Cambio\Report\ReportHistory.rdlc";

            Microsoft.Reporting.WinForms.ReportDataSource table3 = new Microsoft.Reporting.WinForms.ReportDataSource("DataSet3", FaPeso.getHistoryParam());
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(table3);

            this.reportViewer1.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
        }

        public void FacturaIdv()
        {
            //this.Datos();
            //Agregar la plantilla o modelo
            this.reportViewer1.LocalReport.ReportPath = @"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\Casa de Cambio\Report\invoiceReport.rdlc";

            //Microsoft.Reporting.WinForms.ReportParameter parametro1 = new Microsoft.Reporting.WinForms.ReportParameter("rpIDFactura","1");
            //this.reportViewer1.LocalReport.SetParameters(parametro1);

            //this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpIDFactura", "2"));

            //Parametros Factura


            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpIdFactura", model.InvoiceParam.prIdFactura));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpNombreCompleto", model.InvoiceParam.prNombreCompleto));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpCedula", model.InvoiceParam.prCedula));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpMoneda", model.InvoiceParam.prMoneda));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpTasa", model.InvoiceParam.prTasa.ToString()));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpMonto", model.InvoiceParam.prMonto));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpOperacion", model.InvoiceParam.prOperacion));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpResultado", model.InvoiceParam.prResultado.ToString()));
            this.reportViewer1.LocalReport.SetParameters(new Microsoft.Reporting.WinForms.ReportParameter("rpFecha", model.InvoiceParam.prFecha));

            this.reportViewer1.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
        }

        private void Reports_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
