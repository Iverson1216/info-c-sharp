﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casa_de_Cambio.model
{
    public class CoinsReport
    {
        public int prIdCoins { set; get; }
        public string prCoinsNombre { set; get; }
        public decimal prCoinsTasa { set; get; }
    }
}
