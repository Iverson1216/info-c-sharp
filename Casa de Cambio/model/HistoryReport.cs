﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casa_de_Cambio.model
{
    public class HistoryReport
    {
        public int prIdHistory { set; get; }
        public string prNombreCompleto { set; get; }
        public string prCedula { set; get; }
        public string prMoneda { set; get; }
        public decimal prTasa { set; get; }
        public string prMonto { set; get; }
        public string prOperacion { set; get; }
        public decimal prResultado { set; get; }
        public string prFecha { set; get; }
    }
}
