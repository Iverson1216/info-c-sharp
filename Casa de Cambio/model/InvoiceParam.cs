﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casa_de_Cambio.model
{
    public class InvoiceParam
    {      
        public static string prIdFactura { set; get; }       
        public static string prNombreCompleto { set; get; }
        public static string prCedula { set; get; }
        public static string prMoneda { set; get; }
        public static decimal prTasa { set; get; }
        public static string prMonto { set; get; }
        public static string prOperacion { set; get; }
        public static decimal prResultado { set; get; }
        public static string prFecha { set; get; }
    }
}
