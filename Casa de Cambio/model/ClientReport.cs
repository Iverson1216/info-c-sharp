﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casa_de_Cambio.model
{
    public class ClientReport
    {
        public string prIdClient { set; get; }
        public string prClientNombre { set; get; }
        public string prClientCedula { set; get; }
        public string prFecha { set; get; }
    }
}
