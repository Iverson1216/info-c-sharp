﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class NewMoneda : Form
    {
        public NewMoneda()
        {
            InitializeComponent();
        }

        public int idCambio;

        public void dataGrid()
        {
            MySqlCommand mostrar = new MySqlCommand(String.Format("select idCambio,tipoMoneda,valorMoneda from cambio"), Conexion.conectar());
            MySqlDataReader reader = mostrar.ExecuteReader();
            
            this.dgvNewMoneda.Columns.Clear();

            this.dgvNewMoneda.Columns.Add("idCambio", "ID");
            this.dgvNewMoneda.Columns[0].Visible = false;
            this.dgvNewMoneda.Columns.Add("tipoMoneda", "Moneda");
            this.dgvNewMoneda.Columns.Add("valorMoneda", "Tasa de Cambio");

            this.dgvNewMoneda.Rows.Clear();

            if (reader.HasRows)
            {
                this.dgvNewMoneda.Rows.Clear();
                while (reader.Read())
                {
                    int id = int.Parse(reader["idCambio"].ToString());
                    string Moneda = reader["tipoMoneda"].ToString();
                    decimal valor = decimal.Round(decimal.Parse(reader["valorMoneda"].ToString()),2);
                    
                    this.dgvNewMoneda.Rows.Add(id,Moneda, valor);
                }
                Conexion.conectar().Close();
            }
        }

        public static List<model.CoinsReport> getCoinsParam()
        {
            string query = "select idCambio,tipoMoneda,valorMoneda from cambio";
            MySqlCommand mostrar = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = mostrar.ExecuteReader();

            var listaCoins = new List<model.CoinsReport>();

            while (reader.Read())
            {
                model.CoinsReport coins = new model.CoinsReport();
                coins.prIdCoins = int.Parse(reader["idCambio"].ToString());
                coins.prCoinsNombre = reader["tipoMoneda"].ToString();
                coins.prCoinsTasa = decimal.Round(decimal.Parse(reader["valorMoneda"].ToString()),2);
                listaCoins.Add(coins);
            }
            Conexion.conectar().Close();

            return listaCoins;
        }

        public void Agregar()
        {
            string query = $@"insert into cambio (tipoMoneda,valorMoneda) values ('{this.textBox1.Text}',{this.textBox2.Text})";
            MySqlCommand insertar = new MySqlCommand(query,Conexion.conectar());
            int reader = insertar.ExecuteNonQuery();
            Conexion.conectar().Close();
            MessageBox.Show("La moneda se agrego de forma correcta");
            dataGrid();            
        }

        public void Eliminar()
        {
            string query = $@"delete from cambio where idCambio = {this.idCambio}";
            MySqlCommand delete = new MySqlCommand(query,Conexion.conectar());
            delete.ExecuteNonQuery();
            Conexion.conectar().Close();
            MessageBoxButtons boton = MessageBoxButtons.YesNo;
            MessageBoxIcon icono = MessageBoxIcon.Question;
            DialogResult borrar = MessageBox.Show("Esta seguro que desea eliminar esta moneda?","Confirmar",boton,icono);         
            dataGrid();

        }

        public void Actualizar()
        {
            string query = $@"update cambio set tipoMoneda = '{this.textBox1.Text}',valorMoneda = {this.textBox2.Text} where idCambio = {this.idCambio} ";
            MySqlCommand updateDgv = new MySqlCommand(query,Conexion.conectar());
            updateDgv.ExecuteNonQuery();
            Conexion.conectar().Close();
            MessageBox.Show("La moneda ha sido Actualizada");
            dataGrid();
        }
       
        private void NewMoneda_Load(object sender, EventArgs e)
        {
            dataGrid();
            this.iconButton2.Visible = false;
            this.iconButton3.Visible = false;
        }      

        private void iconButton1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == string.Empty || this.textBox2.Text == string.Empty)
            {
                MessageBox.Show("Revise que no halla ningun campo vacio");
            }
            else
            {
                this.Agregar();
                this.idCambio = 0;
                this.textBox1.Clear();
                this.textBox2.Clear();
            }
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {            
            if (idCambio == 0)
            {
                MessageBox.Show("Porfavor elija una moneda antes de usar este boton");
            }
            else
            {
                MessageBoxButtons boton = MessageBoxButtons.YesNo;
                MessageBoxIcon icono = MessageBoxIcon.Question;
                DialogResult borrar = MessageBox.Show("Esta seguro que desea eliminar esta moneda?", "Confirmar", boton, icono);
                if (borrar == DialogResult.Yes)
                {
                    Eliminar();
                this.idCambio = 0;
                this.iconButton1.Visible = true;
                this.iconButton2.Visible = false;
                this.iconButton3.Visible = false;
                }                
            }
                       
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            this.iconButton1.Visible = true;
            this.iconButton2.Visible = false;
            this.iconButton3.Visible = false;

            if (this.textBox1.Text == string.Empty || this.textBox2.Text == string.Empty)
            {
                MessageBox.Show("Revise que no halla ningun campo vacio");
            }
            else
            {
                this.Actualizar();
                this.idCambio = 0;
                this.textBox1.Clear();
                this.textBox2.Clear();
            }
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dgvNewMoneda_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string moneda = this.dgvNewMoneda.CurrentRow.Cells[1].Value.ToString();
            this.textBox1.Text = moneda;

            string valor = this.dgvNewMoneda.CurrentRow.Cells[2].Value.ToString();
            this.textBox2.Text = valor;

            string idMoneda = this.dgvNewMoneda.CurrentRow.Cells[0].Value.ToString();
            idCambio = int.Parse(idMoneda);

            this.iconButton1.Visible = false;
            this.iconButton2.Visible = true;
            this.iconButton3.Visible = true;
        }
    }
}
