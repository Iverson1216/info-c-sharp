﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        public void Registrar()
        {
            string ususario, clave1, clave2;

            ususario = this.textBox1.Text;
            clave1 = this.textBox2.Text;
            clave2 = this.textBox3.Text;

            if (ususario == string.Empty || clave1 == string.Empty || clave2 == string.Empty)
            {
                MessageBox.Show("Resvise que no halla ningun Campo vacio");
            }
            else
            {
                if (clave1 == clave2)
                {
                    MessageBox.Show("se ha registrado de forma exitosa");
                    string query = $@"insert into users(nombre,pass) values ('{this.textBox1.Text}','{this.textBox3.Text}')";
                    MySqlCommand insertar = new MySqlCommand(query, Conexion.conectar());
                    int filas = insertar.ExecuteNonQuery();
                    Conexion.conectar().Close();
                    this.Hide();
                    Form1 inicio = new Form1();
                    inicio.Show();
                }
                else
                {
                    MessageBox.Show("Las claves no coinciden");
                    this.textBox2.Clear();
                    this.textBox3.Clear();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Registrar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 inicio = new Form1();
            inicio.Show();
        }
    }
}
