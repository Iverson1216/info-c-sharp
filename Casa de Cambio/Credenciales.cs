﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class Credenciales : Form
    {
        public Credenciales()
        {
            InitializeComponent();            
        }

        public string idCambio;
        public long LastIdGet;
        public string operacion;
        public double resultado;
        public double tasa;
        public string fecha;

        public void ReportShow()
        {
            model.InvoiceParam.prIdFactura = LastIdGet.ToString();
            model.InvoiceParam.prNombreCompleto = this.textNombre.Text + " " + this.textApellido.Text;
            model.InvoiceParam.prCedula = this.textCedula.Text;
            model.InvoiceParam.prMoneda = this.dgvCambio.CurrentRow.Cells[1].Value.ToString();
            model.InvoiceParam.prTasa = decimal.Round(decimal.Parse(this.dgvCambio.CurrentRow.Cells[2].Value.ToString()),2);
            model.InvoiceParam.prMonto = this.textMonto.Text;
            model.InvoiceParam.prOperacion = operacion;
            model.InvoiceParam.prResultado = decimal.Round(decimal.Parse(resultado.ToString()),2);
            var date = DateTime.Now.ToString("dd-MM-yyyy");                
            model.InvoiceParam.prFecha = date;            
        }

        public void Monedas()
        {
            string query = $@"select idCambio,tipoMoneda as 'moneda', valorMoneda as 'Tasa de cambio' from cambio";
            MySqlCommand moneda = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = moneda.ExecuteReader();
            
            this.dgvCambio.Columns.Clear();
                        
            this.dgvCambio.Columns.Add("idCambio", "ID");
            this.dgvCambio.Columns[0].Visible = false;
            this.dgvCambio.Columns.Add("moneda","Monedas");
            this.dgvCambio.Columns.Add("tasa de cambio","Tasa de Cambio");            

            this.dgvCambio.Rows.Clear();
            
            if (reader.HasRows)
            {
                this.dgvCambio.Rows.Clear();
                while (reader.Read())
                {
                    int id = int.Parse(reader["idCambio"].ToString());
                    string Tmoneda = reader["moneda"].ToString();
                    double valorMoneda = double.Parse(reader["tasa de cambio"].ToString());

                    this.dgvCambio.Rows.Add(id,Tmoneda,valorMoneda);
                }
                Conexion.conectar().Close();
            }
        }

        public void ChangeMoneda()
        {
            string query = $@"insert into factura (idCambio,id_client,monto,fecha_factura) values ({idCambio},{Funciones.idCliente},{this.textMonto.Text},curdate())";
            MySqlCommand cambio = new MySqlCommand(query,Conexion.conectar());
            cambio.ExecuteNonQuery();
            LastIdGet = cambio.LastInsertedId;   
                   
            Conexion.conectar().Close();
            
        }

        public void ChangePeso()
        {
            string query = $@"insert into factura (idCambio,id_client,monto,fecha_factura) values ({idCambio},{Funciones.idCliente},{this.textMonto.Text},curdate())";
            MySqlCommand cambio = new MySqlCommand(query, Conexion.conectar());
            cambio.ExecuteNonQuery();         
            LastIdGet = cambio.LastInsertedId;
            
            string query2 = $@"update factura set operacion = 2 where id = {LastIdGet}";
            MySqlCommand actualizar = new MySqlCommand(query2, Conexion.conectar());
            actualizar.ExecuteNonQuery();
            Console.WriteLine("el ultimo id fue el: " + LastIdGet);
            Conexion.conectar().Close();
        }
       
        private void Credenciales_Load(object sender, EventArgs e)
        {
            //pictureBox1.Image = Image.FromFile(@"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\imges\comision.png");
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            Monedas();
            this.iconButton2.Visible = false;
        } 

        private void btnPC_Click(object sender, EventArgs e)
        {
            this.operacion = this.btnPC.Text;
            this.resultado = double.Parse(this.textMonto.Text) * this.tasa;

            if (this.textNombre.Text == string.Empty || this.textApellido.Text == string.Empty || this.textCedula.Text == string.Empty || this.textMonto.Text == string.Empty)
            {
                MessageBox.Show("Verifique que no hallan campos vacios");
            }
            else
            {
                ChangeMoneda();
                MessageBox.Show("Los datos se agregaron de forma correcta");
                ReportShow();
                Report.Reports r = new Report.Reports();
                r.FacturaIdv();
                this.iconButton2.Visible = true;
                this.textMonto.ReadOnly = true;
                this.btnPC.Visible = false;
                this.btnCP.Visible = false;
                r.Show();             
            }
        }

        private void btnCP_Click(object sender, EventArgs e)
        {
            this.operacion = this.btnCP.Text;
            this.resultado = double.Parse(this.textMonto.Text) / this.tasa;

            if (this.textNombre.Text == string.Empty || this.textApellido.Text == string.Empty || this.textCedula.Text == string.Empty || this.textMonto.Text == string.Empty)
            {
                MessageBox.Show("Verifique que no hallan campos vacios");
            }
            else
            {                                  
                ChangePeso();
                MessageBox.Show("Los datos se agregaron de forma correcta");
                ReportShow();
                Report.Reports r = new Report.Reports();
                r.FacturaIdv();
                this.iconButton2.Visible = true;
                this.textMonto.ReadOnly = true;
                this.btnPC.Visible = false;
                this.btnCP.Visible = false;
                r.Show();
            }        
        }     

        private void dgvCambio_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string moneda = this.dgvCambio.CurrentRow.Cells[1].Value.ToString();
            Funciones.MonedaText = moneda;

            tasa = double.Parse(this.dgvCambio.CurrentRow.Cells[2].Value.ToString());

            string idMoneda = this.dgvCambio.CurrentRow.Cells[0].Value.ToString();
            this.idCambio = idMoneda;

            var ArrayTextBtn = this.btnPC.Text.Split(' ');
            if (ArrayTextBtn.Length > 3)
            {
                ArrayTextBtn[3] = "";
                string newTextoBtn2 = ArrayTextBtn[0] + " " + ArrayTextBtn[1] + " " + Funciones.MonedaText + ArrayTextBtn[3];
                this.btnPC.Text = newTextoBtn2;
            }
            else
            {
                string newTextoBtn = ArrayTextBtn[0] + " " + ArrayTextBtn[1] + " " + Funciones.MonedaText;
                this.btnPC.Text = newTextoBtn;
            }
           

            var ArrayTextbtnCP = this.btnCP.Text.Split(' ');
            if (ArrayTextbtnCP.Length > 3)
            {
                ArrayTextbtnCP[1] = "";
                string newBtnCp2 = Funciones.MonedaText + ArrayTextbtnCP[1] + " " + ArrayTextbtnCP[2] + " " + ArrayTextbtnCP[3];
                this.btnCP.Text = newBtnCp2;
            }
            else
            {
                string newBtnCp = Funciones.MonedaText + " " + ArrayTextbtnCP[1] + " " + ArrayTextbtnCP[2];
                this.btnCP.Text = newBtnCp;
            }
           
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            string cuerpo = "Esta seguro que desea cambiar de Cliente?.";
            string titulo = "Confirm";
            MessageBoxButtons boton = MessageBoxButtons.YesNo;
            MessageBoxIcon mIcon = MessageBoxIcon.Question;
            DialogResult Change = MessageBox.Show(cuerpo,titulo,boton,mIcon);
            if (Change == DialogResult.Yes)
            {
                this.Visible = false;
                Clientes client = new Clientes();
                client.Show();
                Funciones.Montotext = this.textMonto.Text;
            }
        }

        private void Credenciales_FormClosing(object sender, FormClosingEventArgs e)
        {
            Funciones.MonedaText = "?";
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            if (this.textNombre.Text == string.Empty || this.textApellido.Text == string.Empty || this.textCedula.Text == string.Empty || this.textMonto.Text == string.Empty)
            {
                MessageBox.Show("Verifique que no hallan campos vacios");
            }
            else
            {
                FaPeso peso = new FaPeso();
                peso.dataGrid();
                Report.Reports r = new Report.Reports();
                r.FacturaIdv();     
                ReportShow();
                r.Show();
            }         
        }
    }
}
