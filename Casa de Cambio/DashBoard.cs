﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class DashBoard : Form
    {
        public DashBoard()
        {
            InitializeComponent();
        }

        public void graficaMoneda()
        {
            string query = @"SELECT moneda, COUNT(*) as 'cantidad' 
                            FROM queryresultado 
                            GROUP by moneda ORDER by cantidad DESC LIMIT 3";
            MySqlCommand grafica = new MySqlCommand(query,Conexion.conectar());
            MySqlDataReader reader = grafica.ExecuteReader();

            ArrayList Moneda = new ArrayList();
            ArrayList Total = new ArrayList();

            this.topMoneda.Titles.Add("Las 3 monedas mas utilizadas!!");
            this.topMoneda.Series[0].IsValueShownAsLabel = true;
            this.topMoneda.Series[0].ChartType = SeriesChartType.Doughnut;
            this.topMoneda.BackColor = Color.Transparent;
            this.topMoneda.Legends[0].BackColor = Color.Transparent;
            this.topMoneda.ChartAreas[0].BackColor = Color.Transparent;                                                   

            while (reader.Read())
            {
                string tMoneda = reader[0].ToString();
                int tTotal = int.Parse(reader[1].ToString());

                Moneda.Add(tMoneda);
                Total.Add(tTotal);
               
                this.topMoneda.Series[0].Points.DataBindXY(Moneda, Total);
            }
            Conexion.conectar().Close();
        }

        public void graficaClientes()
        {
            string query = @"SELECT nombreCompleto, COUNT(*) as 'cantidad' 
                            FROM queryresultado 
                            GROUP by nombreCompleto ORDER by cantidad DESC LIMIT 3";
            MySqlCommand grafica = new MySqlCommand(query,Conexion.conectar());
            MySqlDataReader reader = grafica.ExecuteReader();

            ArrayList nombreCliente = new ArrayList();
            ArrayList Total = new ArrayList();

            this.topClientes.Titles.Add("Top de los clientes mas frecuentados !!");
            this.topClientes.Series[0].IsValueShownAsLabel = true;
            this.topClientes.Series[0].ChartType = SeriesChartType.Bar;          
            this.topClientes.BackColor = Color.Transparent;            
            this.topClientes.ChartAreas[0].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.NotSet;
            this.topClientes.ChartAreas[0].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.NotSet;
            this.topClientes.Legends[0].BackColor = Color.Transparent;
            this.topClientes.Series[0].IsVisibleInLegend = false;

            while (reader.Read())
            {
                string nombre = reader[0].ToString();
                int tTotal = int.Parse(reader[1].ToString());

                nombreCliente.Add(nombre);
                Total.Add(tTotal);
              
                this.topClientes.Series[0].Points.DataBindXY(nombreCliente,Total);
            }
            Conexion.conectar().Close();
        }

        private void DashBoard_Load(object sender, EventArgs e)
        {
            graficaMoneda();
            graficaClientes();                            
        }

        /*
        public void prueba()
        {
            string[] moneda = { "Dolar", "Euro", "Mexicano","Chino","Gringo","Pakistani"};
            double[] tasa = { 57.20, 74.12, 2.90,88.9,55,12 };

            ArrayList llenaMoneda = new ArrayList();
            ArrayList llenaTasa = new ArrayList();

            //this.grafica.Series.Clear();
            this.grafica.Titles.Add("Top de monedas mas utilizadas");
            this.grafica.Series[0].ChartType = SeriesChartType.Doughnut;
            this.grafica.Series[0].IsValueShownAsLabel = true;
            this.grafica.Series[0].LabelForeColor = Color.Black;
                  
            for (int i = 0; i < moneda.Length; i++)
            {
                llenaMoneda.Add(moneda[i]);
                llenaTasa.Add(tasa[i]);
                grafica.Series[0].Points.DataBindXY(llenaMoneda, llenaTasa);     
            }                      
        }*/
    }
}
