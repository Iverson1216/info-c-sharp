﻿namespace Casa_de_Cambio
{
    partial class FaPeso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FaPeso));
            this.dgvCaP = new System.Windows.Forms.DataGridView();
            this.contextMenuStripHistorial = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.textSearchInvoice = new System.Windows.Forms.TextBox();
            this.iconButton2 = new FontAwesome.Sharp.IconButton();
            this.btnCalendarFilter = new FontAwesome.Sharp.IconPictureBox();
            this.btnCalendarFilterClosed = new FontAwesome.Sharp.IconPictureBox();
            this.DesdeTimerPick = new System.Windows.Forms.DateTimePicker();
            this.HastaTimerPiker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClearBox = new FontAwesome.Sharp.IconPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaP)).BeginInit();
            this.contextMenuStripHistorial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCalendarFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCalendarFilterClosed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearBox)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCaP
            // 
            this.dgvCaP.AllowUserToAddRows = false;
            this.dgvCaP.AllowUserToDeleteRows = false;
            this.dgvCaP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCaP.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCaP.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCaP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaP.ContextMenuStrip = this.contextMenuStripHistorial;
            this.dgvCaP.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCaP.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCaP.Location = new System.Drawing.Point(12, 68);
            this.dgvCaP.Name = "dgvCaP";
            this.dgvCaP.ReadOnly = true;
            this.dgvCaP.RowHeadersVisible = false;
            this.dgvCaP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCaP.Size = new System.Drawing.Size(904, 373);
            this.dgvCaP.TabIndex = 0;
            // 
            // contextMenuStripHistorial
            // 
            this.contextMenuStripHistorial.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showInvoiceToolStripMenuItem});
            this.contextMenuStripHistorial.Name = "contextMenuStripHistorial";
            this.contextMenuStripHistorial.Size = new System.Drawing.Size(139, 26);
            // 
            // showInvoiceToolStripMenuItem
            // 
            this.showInvoiceToolStripMenuItem.Name = "showInvoiceToolStripMenuItem";
            this.showInvoiceToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.showInvoiceToolStripMenuItem.Text = "Show Invoice";
            this.showInvoiceToolStripMenuItem.Click += new System.EventHandler(this.showInvoiceToolStripMenuItem_Click);
            // 
            // iconButton1
            // 
            this.iconButton1.BackColor = System.Drawing.Color.Transparent;
            this.iconButton1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.iconButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.iconButton1.IconColor = System.Drawing.Color.RoyalBlue;
            this.iconButton1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconButton1.Location = new System.Drawing.Point(12, 451);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Size = new System.Drawing.Size(156, 48);
            this.iconButton1.TabIndex = 1;
            this.iconButton1.Text = "New Invoice";
            this.iconButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.iconButton1.UseVisualStyleBackColor = false;
            this.iconButton1.Click += new System.EventHandler(this.iconButton1_Click);
            // 
            // textSearchInvoice
            // 
            this.textSearchInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textSearchInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSearchInvoice.Location = new System.Drawing.Point(515, 33);
            this.textSearchInvoice.Name = "textSearchInvoice";
            this.textSearchInvoice.Size = new System.Drawing.Size(300, 26);
            this.textSearchInvoice.TabIndex = 14;
            this.textSearchInvoice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textSearchInvoice_KeyUp);
            // 
            // iconButton2
            // 
            this.iconButton2.BackColor = System.Drawing.Color.Transparent;
            this.iconButton2.FlatAppearance.BorderSize = 0;
            this.iconButton2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.iconButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton2.IconChar = FontAwesome.Sharp.IconChar.Search;
            this.iconButton2.IconColor = System.Drawing.Color.RoyalBlue;
            this.iconButton2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconButton2.Location = new System.Drawing.Point(819, 28);
            this.iconButton2.Name = "iconButton2";
            this.iconButton2.Size = new System.Drawing.Size(39, 41);
            this.iconButton2.TabIndex = 15;
            this.iconButton2.UseVisualStyleBackColor = false;
            this.iconButton2.Click += new System.EventHandler(this.iconButton2_Click);
            // 
            // btnCalendarFilter
            // 
            this.btnCalendarFilter.BackColor = System.Drawing.Color.Transparent;
            this.btnCalendarFilter.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCalendarFilter.IconChar = FontAwesome.Sharp.IconChar.CalendarAlt;
            this.btnCalendarFilter.IconColor = System.Drawing.Color.RoyalBlue;
            this.btnCalendarFilter.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCalendarFilter.IconSize = 48;
            this.btnCalendarFilter.Location = new System.Drawing.Point(858, 25);
            this.btnCalendarFilter.Name = "btnCalendarFilter";
            this.btnCalendarFilter.Size = new System.Drawing.Size(51, 44);
            this.btnCalendarFilter.TabIndex = 16;
            this.btnCalendarFilter.TabStop = false;
            this.btnCalendarFilter.Click += new System.EventHandler(this.btnCalendarFilter_Click);
            // 
            // btnCalendarFilterClosed
            // 
            this.btnCalendarFilterClosed.BackColor = System.Drawing.Color.Transparent;
            this.btnCalendarFilterClosed.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCalendarFilterClosed.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.btnCalendarFilterClosed.IconColor = System.Drawing.Color.RoyalBlue;
            this.btnCalendarFilterClosed.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCalendarFilterClosed.IconSize = 30;
            this.btnCalendarFilterClosed.Location = new System.Drawing.Point(902, 3);
            this.btnCalendarFilterClosed.Name = "btnCalendarFilterClosed";
            this.btnCalendarFilterClosed.Size = new System.Drawing.Size(32, 26);
            this.btnCalendarFilterClosed.TabIndex = 17;
            this.btnCalendarFilterClosed.TabStop = false;
            this.btnCalendarFilterClosed.Click += new System.EventHandler(this.btnCalendarFilterClosed_Click);
            // 
            // DesdeTimerPick
            // 
            this.DesdeTimerPick.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DesdeTimerPick.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DesdeTimerPick.Location = new System.Drawing.Point(539, 33);
            this.DesdeTimerPick.Name = "DesdeTimerPick";
            this.DesdeTimerPick.Size = new System.Drawing.Size(106, 27);
            this.DesdeTimerPick.TabIndex = 18;
            this.DesdeTimerPick.ValueChanged += new System.EventHandler(this.DesdeTimerPick_ValueChanged);
            // 
            // HastaTimerPiker
            // 
            this.HastaTimerPiker.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HastaTimerPiker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.HastaTimerPiker.Location = new System.Drawing.Point(707, 34);
            this.HastaTimerPiker.Name = "HastaTimerPiker";
            this.HastaTimerPiker.Size = new System.Drawing.Size(108, 27);
            this.HastaTimerPiker.TabIndex = 19;
            this.HastaTimerPiker.ValueChanged += new System.EventHandler(this.HastaTimerPiker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(474, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 24);
            this.label1.TabIndex = 20;
            this.label1.Text = "Desde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(648, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 24);
            this.label2.TabIndex = 21;
            this.label2.Text = "Hasta";
            // 
            // btnClearBox
            // 
            this.btnClearBox.BackColor = System.Drawing.Color.Transparent;
            this.btnClearBox.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnClearBox.IconChar = FontAwesome.Sharp.IconChar.TrashAlt;
            this.btnClearBox.IconColor = System.Drawing.Color.RoyalBlue;
            this.btnClearBox.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnClearBox.IconSize = 30;
            this.btnClearBox.Location = new System.Drawing.Point(901, 36);
            this.btnClearBox.Name = "btnClearBox";
            this.btnClearBox.Size = new System.Drawing.Size(32, 26);
            this.btnClearBox.TabIndex = 22;
            this.btnClearBox.TabStop = false;
            this.btnClearBox.Click += new System.EventHandler(this.btnClearBox_Click);
            // 
            // FaPeso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(928, 510);
            this.Controls.Add(this.btnClearBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HastaTimerPiker);
            this.Controls.Add(this.DesdeTimerPick);
            this.Controls.Add(this.btnCalendarFilterClosed);
            this.Controls.Add(this.textSearchInvoice);
            this.Controls.Add(this.iconButton1);
            this.Controls.Add(this.dgvCaP);
            this.Controls.Add(this.iconButton2);
            this.Controls.Add(this.btnCalendarFilter);
            this.Controls.Add(this.label1);
            this.Name = "FaPeso";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FaPeso";
            this.Load += new System.EventHandler(this.FaPeso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaP)).EndInit();
            this.contextMenuStripHistorial.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCalendarFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCalendarFilterClosed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClearBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCaP;
        private FontAwesome.Sharp.IconButton iconButton1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripHistorial;
        private System.Windows.Forms.ToolStripMenuItem showInvoiceToolStripMenuItem;
        private System.Windows.Forms.TextBox textSearchInvoice;
        private FontAwesome.Sharp.IconButton iconButton2;
        private FontAwesome.Sharp.IconPictureBox btnCalendarFilter;
        private FontAwesome.Sharp.IconPictureBox btnCalendarFilterClosed;
        private System.Windows.Forms.DateTimePicker DesdeTimerPick;
        private System.Windows.Forms.DateTimePicker HastaTimerPiker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private FontAwesome.Sharp.IconPictureBox btnClearBox;
    }
}