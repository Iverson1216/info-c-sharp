﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Casa_de_Cambio
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();         
            this.label2.Text = Funciones.UserLogin;          
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            this.Hide();
            form.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string cuerpo = "Esta seguro que desea salir?";
            string titulo = "Confirm";
            MessageBoxButtons boton = MessageBoxButtons.YesNo;
            MessageBoxIcon bIcon = MessageBoxIcon.Question;
            DialogResult salir = MessageBox.Show(cuerpo,titulo,boton,bIcon);
            if (salir == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void clientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clientes client = new Clientes();
            client.Show();
        }

        private void facturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Credenciales cre = new Credenciales();
            cre.Show();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewMoneda moneda = new NewMoneda();
            moneda.Show();
        }

        private void printReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Reporte factura individual
            Report.Reports r = new Report.Reports();
            r.FacturaIdv();
            r.Show();
        }    

        private void fileToolStripMenuItem_MouseHover(object sender, EventArgs e)
        {
            this.fileToolStripMenuItem.ForeColor = Color.Black;
        }

        private void fileToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            this.fileToolStripMenuItem.ForeColor = Color.White;
        }

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FaPeso peso = new FaPeso();
            peso.Show();
        }

        private void reportClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Reportes de Cliente
            Report.Reports r = new Report.Reports();
            r.clientes();
            r.Show();           
        }

        private void reportCoinsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Reportes de Monedas
            Report.Reports r = new Report.Reports();
            r.monedas();
            r.Show();
        }

        private void reportHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Reporte del historial general
            Report.Reports r = new Report.Reports();
            r.Historial();
            r.Show();
        }

        private void dashBoartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DashBoard dash = new DashBoard();
            dash.Show();
        }
    }
}
