﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class Form1 : Form 
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void Login()
        {
            string query = $@"Select * from users where nombre = '{this.userText.Text}' and pass = '{this.textBox2.Text}' ";
            MySqlCommand loginCommand = new MySqlCommand(query,Conexion.conectar());
            MySqlDataReader reader = loginCommand.ExecuteReader();

            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    object nombre, pass;

                    nombre = reader["nombre"].ToString();
                    pass = reader["pass"].ToString();

                    Funciones.UserLogin = nombre.ToString();                  

                    this.Hide();   
                    Menu menu = new Menu();                    
                    menu.Show();

                }
                MessageBox.Show("Usuario y clave de acceso correcta");
            }
            else
            {
                MessageBox.Show("El usuario o clave es incorrecta");
                
            }                      
            Conexion.conectar().Close();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {                
            this.Login();                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Register registrar = new Register();
            registrar.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string cuerpo = "Esta seguro que desea salir?";
            string titulo = "Confirmar";
            MessageBoxButtons boton = MessageBoxButtons.YesNo;
            MessageBoxIcon iconos = MessageBoxIcon.Question;

            DialogResult salir = MessageBox.Show(cuerpo,titulo,boton,iconos);
            if (salir == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Login();
            }
        }
    }
}
