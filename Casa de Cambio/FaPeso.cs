﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Casa_de_Cambio
{
    public partial class FaPeso : Form
    {
        public FaPeso()
        {
            InitializeComponent();
        }

        private DateTime Desde = new DateTime(1900, 1, 1);
        private DateTime Hasta = new DateTime(3000, 1, 1);             

        public void dataGrid()
        {
            string query = $@"SELECT id,
                                    nombreCompleto,
                                    cedula,
                                    moneda,
                                    tasa,
                                    monto,
                                    operacion,
                                    resultado,
                                    date_format(fecha, '%d-%m-%Y') as fecha FROM queryresultado
                                    WHERE
                                    fecha >= '{this.Desde.ToString("yyyy-MM-dd")}' and fecha <= '{this.Hasta.ToString("yyyy-MM-dd")}' and nombreCompleto LIKE '%{this.textSearchInvoice.Text}%' ORDER BY id ASC";
            MySqlCommand mostrar = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = mostrar.ExecuteReader();

            this.dgvCaP.Columns.Clear();

            this.dgvCaP.Columns.Add("id","ID");
            //this.dgvCaP.Columns[0].Visible = false;
            this.dgvCaP.Columns.Add("nombreCompleto", "Nombre Completo");
            this.dgvCaP.Columns.Add("cedula", "Cedula");
            this.dgvCaP.Columns.Add("tipoMoneda", "Moneda");
            this.dgvCaP.Columns.Add("valorMoneda", "Tasa de Cambio");
            this.dgvCaP.Columns.Add("monto", "Monto");
            this.dgvCaP.Columns.Add("operacion", "Operacion");
            this.dgvCaP.Columns.Add("resultado", "Resultado");
            this.dgvCaP.Columns.Add("fecha", "Fecha");

            this.dgvCaP.Rows.Clear();

            if (reader.HasRows)
            {
                while (reader.Read())
                {                    
                    string idInvoice = reader["id"].ToString();
                    string nombreC = reader["nombreCompleto"].ToString();
                    string Cedula = reader["cedula"].ToString();
                    string Moneda = reader["moneda"].ToString();
                    decimal valor = decimal.Round(decimal.Parse(reader["tasa"].ToString()),2);                 
                    int monto = int.Parse(reader["monto"].ToString());                    
                    string operacion = reader["operacion"].ToString();
                    decimal resultado = decimal.Round(decimal.Parse(reader["resultado"].ToString()),2);
                    string Fecha = reader["fecha"].ToString();                  

                    this.dgvCaP.Rows.Add(idInvoice, nombreC, Cedula, Moneda, valor, monto, operacion, resultado, Fecha);
                }
                Conexion.conectar().Close();
            }
        }    
        
        public static List<model.HistoryReport> getHistoryParam()
        {
            string query = $@"SELECT id,
                                    nombreCompleto,
                                    cedula,
                                    moneda,
                                    tasa,
                                    monto,
                                    operacion,
                                    resultado,
                                    date_format(fecha, '%d-%m-%Y') as fecha FROM queryresultado ORDER by id ASC";
            MySqlCommand mostrar = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = mostrar.ExecuteReader();

            var listasHistory = new List<model.HistoryReport>();
            while (reader.Read())
            {
                model.HistoryReport history = new model.HistoryReport();

                history.prIdHistory = int.Parse(reader["id"].ToString());
                history.prNombreCompleto = reader["nombreCompleto"].ToString();
                history.prCedula = reader["cedula"].ToString();
                history.prMoneda = reader["moneda"].ToString();
                history.prTasa = decimal.Round(decimal.Parse(reader["tasa"].ToString()),2);
                history.prMonto = reader["monto"].ToString();
                history.prOperacion = reader["operacion"].ToString();
                history.prResultado = decimal.Round(decimal.Parse(reader["resultado"].ToString()),2);
                history.prFecha = reader["fecha"].ToString();

                listasHistory.Add(history);
            }
            Conexion.conectar().Close();

            return listasHistory;
        }         

        private void FaPeso_Load(object sender, EventArgs e)
        {
            dataGrid();
            this.btnCalendarFilterClosed.Visible = false;
            this.DesdeTimerPick.Visible = false;
            this.HastaTimerPiker.Visible = false;
            this.label1.Visible = false;
            this.label2.Visible = false;        
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            Clientes client = new Clientes();
            this.Hide();          
            client.Show();
        }
   
        private void showInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            model.InvoiceParam.prIdFactura = this.dgvCaP.CurrentRow.Cells[0].Value.ToString();
            model.InvoiceParam.prNombreCompleto = this.dgvCaP.CurrentRow.Cells[1].Value.ToString();
            model.InvoiceParam.prCedula = this.dgvCaP.CurrentRow.Cells[2].Value.ToString();
            model.InvoiceParam.prMoneda = this.dgvCaP.CurrentRow.Cells[3].Value.ToString();
            model.InvoiceParam.prTasa = decimal.Round(decimal.Parse(this.dgvCaP.CurrentRow.Cells[4].Value.ToString()),2);
            model.InvoiceParam.prMonto = this.dgvCaP.CurrentRow.Cells[5].Value.ToString();
            model.InvoiceParam.prOperacion = this.dgvCaP.CurrentRow.Cells[6].Value.ToString();
            model.InvoiceParam.prResultado = decimal.Round(decimal.Parse(this.dgvCaP.CurrentRow.Cells[7].Value.ToString()),2);
            model.InvoiceParam.prFecha = this.dgvCaP.CurrentRow.Cells[8].Value.ToString();

            Report.Reports r = new Report.Reports();
            r.FacturaIdv();
            r.Show();       
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            dataGrid();
        }

        private void textSearchInvoice_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dataGrid();
            }
        }

        private void btnCalendarFilter_Click(object sender, EventArgs e)
        {
            this.textSearchInvoice.Visible = false;
            this.DesdeTimerPick.Visible = true;
            this.HastaTimerPiker.Visible = true;
            this.label1.Visible = true;
            this.label2.Visible = true;
            this.btnCalendarFilterClosed.Visible = true;     
        }

        private void btnCalendarFilterClosed_Click(object sender, EventArgs e)
        {
            this.DesdeTimerPick.Visible = false;
            this.HastaTimerPiker.Visible = false;
            this.label1.Visible = false;
            this.label2.Visible = false;
            this.textSearchInvoice.Visible = true;            
        }

        private void DesdeTimerPick_ValueChanged(object sender, EventArgs e)
        {
            Desde = this.DesdeTimerPick.Value;
            Console.WriteLine("la fecha es: " + Desde);
        }

        private void HastaTimerPiker_ValueChanged(object sender, EventArgs e)
        {
            Hasta = this.HastaTimerPiker.Value;          
        }

        private void btnClearBox_Click(object sender, EventArgs e)
        {
            this.textSearchInvoice.Clear();
            this.DesdeTimerPick.ResetText();
            this.HastaTimerPiker.ResetText();
            this.Desde = new DateTime(1900, 1, 1);
            this.Hasta = new DateTime(3000, 1, 1);
            dataGrid();          
        }
    }
}
