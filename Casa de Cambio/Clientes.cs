﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Casa_de_Cambio
{
    public partial class Clientes : Form
    {
        public Clientes()
        {
            InitializeComponent();
        }        
        private DateTime Desde = new DateTime(1900,01,01);
        private DateTime Hasta = new DateTime(3000,01,01);
        
        public void DataClient()
        {
            string query = $@"select id_client,
                                    nombre,apellido,concat(nombre, ' ' ,apellido) as 'nombreCompleto',
                                    cedula,
                                    date_format(fecha_client,'%d-%m-%Y') as 'fecha_client' 
                                    from client 
                                    where fecha_client between '{Desde.ToString("yyyy-MM-dd")}' and '{Hasta.ToString("yyyy-MM-dd")}' and concat(nombre, ' ' ,apellido) like '%{this.textSearch.Text}%'";
            MySqlCommand client = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = client.ExecuteReader();           

            this.dgvClient.Columns.Clear();

            this.dgvClient.Columns.Add("id_client", "ID");
            this.dgvClient.Columns[0].Visible = false;
            this.dgvClient.Columns.Add("nombre", "Nombre");
            this.dgvClient.Columns[1].Visible = false;
            this.dgvClient.Columns.Add("apellido", "Apellido");
            this.dgvClient.Columns[2].Visible = false;
            this.dgvClient.Columns.Add("nombreCompleto", "Nombre Completo");
            this.dgvClient.Columns.Add("cedula", "Cedula");
            this.dgvClient.Columns.Add("fecha_client", "Fecha");

            this.dgvClient.Rows.Clear();

            if (reader.HasRows)
            {        
                while (reader.Read())
                {
                    int idFactura = int.Parse(reader["id_client"].ToString());
                    string Nombre = reader["nombre"].ToString();
                    string Apellido = reader["apellido"].ToString();
                    string NombreCompleto = reader["nombreCompleto"].ToString();                            
                    string Cedula = reader["cedula"].ToString();
                    string Fecha = reader["fecha_client"].ToString();                              

                    this.dgvClient.Rows.Add(idFactura,Nombre,Apellido,NombreCompleto,Cedula,Fecha);
                }
                Conexion.conectar().Close();
            }
        }
        
        public static List<model.ClientReport> getListCliente() {
            string query = $@"select id_client,
                                    nombre,apellido,concat(nombre, ' ' ,apellido) as 'nombreCompleto',
                                    cedula,
                                    date_format(fecha_client,'%d-%m-%Y') as 'fecha_client' 
                                    from client";
            MySqlCommand client = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = client.ExecuteReader();

            var listaClient = new List<model.ClientReport>();
            while (reader.Read())
            {
                model.ClientReport clientes = new model.ClientReport();
                clientes.prIdClient = reader["id_client"].ToString();
                clientes.prClientNombre = reader["nombreCompleto"].ToString();
                clientes.prClientCedula = reader["cedula"].ToString();
                clientes.prFecha = reader["fecha_client"].ToString();
                /*
                model.ClientReport ejemplo = new model.ClientReport() {
                    prIdClient = "1",
                    prClientCedula = "333223234234",
                    prClientNombre = "nombre"

                };
                model.ClientReport ejemplo1 = new model.ClientReport {
                    prClientNombre = "",
                    prClientCedula = "",
                    prIdClient = ""
                };

                listaClient.Add(new model.ClientReport {
                    prIdClient = "",
                    prClientCedula = "",
                    prClientNombre = ""
                });
                */
               listaClient.Add(clientes);
            }
                Conexion.conectar().Close();
            
            return listaClient;
        }
        
        public bool validacionCedula()
        {
            string query = $@"select cedula from client where cedula = '{this.maskedTextBox1.Text}'";
            MySqlCommand client = new MySqlCommand(query, Conexion.conectar());
            MySqlDataReader reader = client.ExecuteReader();
            return reader.HasRows;
        }

        public void agregar()
        {
            string query = $@"insert into client (nombre,apellido,cedula,fecha_client) values ('{this.textCnombre.Text}','{this.textCapellido.Text}','{this.maskedTextBox1.Text}',curdate())";
            Console.WriteLine(query);
            MySqlCommand adicionar = new MySqlCommand(query,Conexion.conectar());
            adicionar.ExecuteNonQuery();
            MessageBox.Show("El Cliente se agrego de forma exitosa.");
        }

        public void borrar()
        {
            string query = $@"delete from client where id_client = {Funciones.idCliente}";
            MySqlCommand del = new MySqlCommand(query, Conexion.conectar());
            del.ExecuteNonQuery();
            MessageBox.Show("El Cliente se elimino de forma Exitosa.");
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            this.DataClient();
            this.iconButton2.Visible = false;
            this.iconButton3.Visible = false;
            this.label4.Visible = false;
            this.label5.Visible = false;
            this.dtpDesde.Visible = false;
            this.dtpHasta.Visible = false;
            this.btnCloseCalendario.Visible = false;           
        }

        private void dgvClient_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sIdClient = this.dgvClient.CurrentRow.Cells[0].Value.ToString();
            Funciones.idCliente = int.Parse(sIdClient);

            string sNombre = this.dgvClient.CurrentRow.Cells[1].Value.ToString();
            this.textCnombre.Text = sNombre;

            string sApellido = this.dgvClient.CurrentRow.Cells[2].Value.ToString();
            this.textCapellido.Text = sApellido;

            string cell = this.dgvClient.CurrentRow.Cells[4].Value.ToString();
            this.maskedTextBox1.Text = cell;                       

            this.iconButton2.Visible = true;
            this.iconButton3.Visible = true;
            this.iconButton1.Visible = false;         
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            if (this.textCnombre.Text == string.Empty || this.textCapellido.Text == string.Empty || this.maskedTextBox1.Text == string.Empty)
            {
                MessageBox.Show("Revise que no halla campos vacios.");
            }
            else if (validacionCedula())
            {
                MessageBox.Show("La cedula ya existe.");
            }
            else 
            {
                this.agregar();
                this.DataClient();
                this.textCnombre.Clear();
                this.textCapellido.Clear();
                this.maskedTextBox1.Clear();
            }           
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            if (this.textCnombre.Text == string.Empty || this.textCapellido.Text == string.Empty || this.maskedTextBox1.Text == string.Empty)
            {
                MessageBox.Show("Revise que no halla campos vacios");
            }
            else
            {
                Credenciales c = new Credenciales();
                if (c.Visible == false)
                {                    
                    c.textNombre.Text = this.textCnombre.Text;
                    c.textApellido.Text = this.textCapellido.Text;
                    c.textCedula.Text = this.maskedTextBox1.Text;

                    c.textMonto.Text = Funciones.Montotext;

                    var ArrayTextBtn = c.btnPC.Text.Split(' ');
                    string newTextoBtn = ArrayTextBtn[0] + " " + ArrayTextBtn[1] + " " + Funciones.MonedaText;
                    c.btnPC.Text = newTextoBtn;

                    var ArrayTextbtnCP = c.btnCP.Text.Split(' ');
                    string newBtnCp = Funciones.MonedaText + " " + ArrayTextbtnCP[1] + " " + ArrayTextbtnCP[2];
                    c.btnCP.Text = newBtnCp;

                    this.Hide();
                    c.Visible = true;
                }
                else
                {
                    c.textNombre.Text = this.textCnombre.Text;
                    c.textApellido.Text = this.textCapellido.Text;
                    c.textCedula.Text = this.maskedTextBox1.Text;
                    this.Hide();
                    c.Show();
                }
            }
            
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            if (this.textCnombre.Text == string.Empty || this.textCapellido.Text == string.Empty || this.maskedTextBox1.Text == string.Empty)
            {
                MessageBox.Show("Revise que no hallan campos vacios.");
            }
            else
            {
                this.borrar();
                this.DataClient();
                this.textCnombre.Clear();
                this.textCapellido.Clear();
                this.maskedTextBox1.Clear();
            }          
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            this.textCnombre.Clear();
            this.textCapellido.Clear();
            this.maskedTextBox1.Clear();
            this.iconButton1.Visible = true;
            this.iconButton2.Visible = false;
            this.iconButton3.Visible = false;
        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            DataClient();
        }      

        private void textSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DataClient();
            }
        }

        private void btnCalendario_Click(object sender, EventArgs e)
        {
            this.textSearch.Visible = false;
            this.label4.Visible = true;
            this.label5.Visible = true;
            this.dtpDesde.Visible = true;
            this.dtpHasta.Visible = true;
            this.btnCloseCalendario.Visible = true;       
        }

        private void btnCloseCalendario_Click(object sender, EventArgs e)
        {
            this.textSearch.Visible = true;
            this.label4.Visible = false;
            this.label5.Visible = false;
            this.dtpDesde.Visible = false;
            this.dtpHasta.Visible = false;
            this.btnCloseCalendario.Visible = false;                     
        }

        private void dtpDesde_ValueChanged(object sender, EventArgs e)
        {
            Desde = this.dtpDesde.Value;
            Console.WriteLine("La fechas es: " + Desde.ToString("yyyy-MM-dd"));        
        }

        private void dtpHasta_ValueChanged(object sender, EventArgs e)
        {
            Hasta = this.dtpHasta.Value;
            Console.WriteLine("La fechas es: " + Hasta.ToString("yyyy-MM-dd"));
        }

        private void btnClearBox_Click(object sender, EventArgs e)
        {
            this.textSearch.Clear();
            this.dtpDesde.ResetText();
            this.dtpHasta.ResetText();
            this.Desde = new DateTime(1900, 1, 1);
            this.Hasta = new DateTime(3000, 1, 1);
            DataClient();
        }
    }
}
