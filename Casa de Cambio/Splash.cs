﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Casa_de_Cambio
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile(@"C:\Users\WEROM\Documents\Casa de Cambio\info-c-sharp\imges\pramideSplash.gif");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;          
            this.timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Stop();                
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }
    }
}
